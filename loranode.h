#ifndef __loranode__
#define __loranode__

#include <RH_RF95.h>
// use this extern to use rf95 in ino file until final merge of loranode with radiohead lib
extern RH_RF95 rf95;

#define debugSerial true

#define debugPrintLn(...) { if (debugSerial) Serial.println(__VA_ARGS__); }
#define debugPrint(...) { if (debugSerial) Serial.print(__VA_ARGS__); }
#define debugSerialBegin(...) {if (debugSerial) Serial.begin(__VA_ARGS__); 	while (!Serial);}
#define debugSerialFlush() {if (debugSerial) Serial.flush(); }

// must write each bands settings beforehand here
enum bands {g=0};
#define NUM_BANDS 1

const float band_start_freq[NUM_BANDS] = {868.1};
const uint8_t band_num_channels[NUM_BANDS] = {3};

class loranode {

  public:
	loranode();
	loranode(uint8_t bid, uint8_t chan, RH_RF95::BW_IH_t bw_ih_conf, RH_RF95::SF_t sf_conf, RH_RF95::LDRO_AGC_t ldro_agc_conf, uint8_t pow);

	static struct band{
		uint8_t band_id;
		uint8_t num_channels;
		float *channel_freq; // MHz
	}my_band[NUM_BANDS];

	void init();
	static void setup_band(uint8_t band_id, float start_freq, uint8_t num_channels, float channel_spacing_mhz);

	/* Getters-setters */
	uint8_t getBandId() const;
	void setBandId(uint8_t bandId);

	void setFreq(uint8_t bandId, uint8_t chan);

	uint8_t getChannel() const;
	void setChannel(uint8_t channel);

	uint8_t getPower() const;
	void setPower(uint8_t power);

	RH_RF95::BW_IH_t getBwIhConf() const;
	void setBwIhConf(RH_RF95::BW_IH_t bwIhConf);

	RH_RF95::SF_t getSfConf() const;
	void setSfConf(RH_RF95::SF_t sfConf);

	RH_RF95::LDRO_AGC_t getLdroAgcConf() const;
	void setLdroAgcConf(RH_RF95::LDRO_AGC_t ldroAgcConf);

	/* Rest of the functions */
	void setNextSf();


  private:

	uint8_t band_id;
	uint8_t channel;
	RH_RF95::BW_IH_t bw_ih_conf;
	RH_RF95::SF_t sf_conf;
	RH_RF95::LDRO_AGC_t ldro_agc_conf;
	uint8_t power;
};

#endif
