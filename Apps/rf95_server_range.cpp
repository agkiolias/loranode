// rf95_server range metering
// -*- mode: C++ -*-

#include "FS.h"
#include <SPI.h>
#include <loranode.h>

const String fp = "/measurements.txt";

loranode lnode;

void print_file(File f);
void getSF(RH_RF95::SF_t config, char* x);
File f;

void setup() {

	debugSerialBegin(115200);
	while (!Serial) ; // Wait for serial port to be available

	// always use this to "mount" the filesystem,  SPIFFS.end() to unmount
	if( SPIFFS.begin() == true){
		debugPrintLn("File system was mounted successfully!");
	}
	else{
		debugPrintLn("File system mount error! Exiting.");
		return;
	}

	if (SPIFFS.exists(fp) == true){

		if( SPIFFS.remove(fp) == true){
			debugPrintLn("File removed successfully!");
		}
		else{
			debugPrintLn("File not removed.");
		}
	}
	else{
		debugPrintLn("File doesn't exist!");
	}

	// open the file in append mode
	f = SPIFFS.open(fp, "a+");
	f.println("==Start==");

	// open the file in read mode to move current index at the beginning
	//f = SPIFFS.open(fp, "r");

	/*
	String debugLogData;

	f.seek(0, SeekSet);
	while (f.available()){

	//Serial.println(f.position());
	debugLogData += char(f.read());
	}
	f.close();

	Serial.println("=====================================");
	Serial.println(debugLogData);
	Serial.println("=====================================");

	*/

	// display the / folder contents
	Dir dir = SPIFFS.openDir("/");
	while (dir.next()) {
		debugPrint(dir.fileName());
		File f = dir.openFile("r");
		debugPrint(" -> ");
		debugPrint(f.size());
		debugPrintLn(" bytes");
		f.close();
	}

    lnode.init();

	debugSerialFlush();
}

void loop() {

	if (rf95.waitAvailableTimeout(1500))
	  {
	    // Should be a message for us now
	    uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
	    uint8_t len = sizeof(buf);
	    if (rf95.recv(buf, &len))
	    {
		  // Send a reply
		  uint8_t data[] = "A";

		  rf95.send(data, sizeof(data));
		  rf95.waitPacketSent();

	      float flat;
	      float flon;

	      memcpy(&flat, buf, sizeof(float));
	      memcpy(&flon, buf+4, sizeof(float));

		  char buffer [50];
		  char sf[3];
		  getSF( lnode.getSfConf(),sf );
		  sprintf(buffer, "SF%s: %f,%f up: %d down: ",sf ,flat,flon,rf95.lastRssi() );

		  f.print(buffer);

		  if (rf95.waitAvailableTimeout(2000))
			{
		  	    if (rf95.recv(buf, &len))
		  	    {
		  	    	int16_t down_rssi;
		  	      	memcpy(&down_rssi, buf, sizeof(int16_t));

		  	      	f.println(down_rssi, DEC);
		  	    }
				else
				{
					f.println('-', DEC);
					debugPrintLn("recv failed");
				}
			}
		  print_file(f);

		  lnode.setNextSf();
	    }
	    else
	    {
	      Serial.println("first recv failed");
	    }
	  }
	// in case of a lost transmission, move to next SF after the timeout
	else if(lnode.getSfConf() < RH_RF95::Sf12_CRC){

		lnode.setNextSf();
	}

	debugSerialFlush();
}

void print_file(File f){

	// print all file contents
	debugPrintLn("Printing file..");

	String FileData;

	f.seek(0, SeekSet);
	while(f.available()) {
		//Lets read char by char from the file
		FileData += (char)f.read();

		//Lets read line by line from the file
		//FileData += f.readStringUntil('\n');
	}
	debugPrintLn(FileData);
	//f.close();
}

void getSF(RH_RF95::SF_t config, char* x){

	if (config == RH_RF95::Sf12_CRC){
		sprintf(x,"12");
	}
	else if (config == RH_RF95::Sf11_CRC){
		sprintf(x,"11");
	}
	else if (config == RH_RF95::Sf10_CRC){
		sprintf(x,"10");
	}
	else if (config == RH_RF95::Sf9_CRC){
		sprintf(x,"9");
	}
	else if (config == RH_RF95::Sf8_CRC){
		sprintf(x,"8");
	}
	else if (config == RH_RF95::Sf7_CRC){
		sprintf(x,"7");
	}
	else{
		sprintf(x,"6");
	}
}
