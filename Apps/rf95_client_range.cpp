// rf95_client range metering
// -*- mode: C++ -*-

#include <SPI.h>
#include <loranode.h>
#include <SoftwareSerial.h>
#include <TinyGPS.h>

#define ATM328

loranode lnode;

// create gps related objects
TinyGPS gps;
SoftwareSerial ss(4, 3);

static void smartdelay(unsigned long ms);

void setup()
{
	debugSerialBegin(115200);
	while (!Serial) ; // Wait for serial port to be available
    ss.begin(9600);

    smartdelay(1000);

    lnode.init();

	/*
	if (!rf95.init()){
		debugPrintLn("init failed");
	}

	// Set BW, CR, SF
	rf95.setBW_IH( lnode.getBwIhConf());
	rf95.setLDRO_AGC( lnode.getLdroAgcConf());
	rf95.setSf( lnode.getSfConf());

	debugPrint("Initial config = ");
	//debugPrintLn( lnode.getModemConf() );

	// Set Tx freq
	float node_freq = loranode::my_band[lnode.getBandId()].channel_freq[lnode.getChannel()];
	rf95.setFrequency( node_freq );

	debugPrint("Initial freq = ");
	debugPrintLn( node_freq );

	// Set Power,dBm
	rf95.setTxPower( lnode.getPower(), false ); // from 5 to 23 dBm using PA_BOOST for RFM95/96/97/98 modules

	debugPrint("Initial power = ");
	debugPrintLn( lnode.getPower() );
	*/
	debugSerialFlush();
}

void loop()
{
	float flat, flon;
	unsigned long age;
	gps.f_get_position(&flat, &flon, &age);

	debugPrintLn("CLIENT: transmitting coords");
	debugPrint("COORDS: ");
	debugPrint("(LATIDUDE, LONGITUDE) = (");
	debugPrint(flat,6);
	debugPrint(", ");
	debugPrint(flon,6);
	debugPrintLn(")");

	// Send a message to rf95_server
	unsigned char coords[8];
	memcpy(coords, &flat, 4);
	memcpy(coords+4, &flon, 4);

	unsigned char down_rssi[2];

	for ( int sf = RH_RF95::Sf12_CRC; sf >= RH_RF95::Sf7_CRC; sf-- ){

		delay(200); // needed to offset the time needed for print_file(f) of server

		// move to next DR
		RH_RF95::SF_t m_conf = static_cast<RH_RF95::SF_t> (sf) ;
		lnode.setSfConf( m_conf );

		// transmit coords to server - server records coords + client rssi
		rf95.send(coords, sizeof(coords));
		rf95.waitPacketSent();

		// Now wait for a reply
		uint8_t buf[RH_RF95_MAX_MESSAGE_LEN];
		uint8_t len = sizeof(buf);

		if (rf95.waitAvailableTimeout(2000))
		{
			// received message from server
			if (rf95.recv(buf, &len))
			{
			  debugPrint("RSSI: ");
			  debugPrintLn(rf95.lastRssi(), DEC);

			  int16_t lastRssi = rf95.lastRssi();
			  memcpy(down_rssi, &lastRssi, sizeof(int16_t));

			  delay(200);
			  // transmit server rssi - server records its rssi as well
			  rf95.send( down_rssi, sizeof(int16_t) );
			  rf95.waitPacketSent();
			}
			else
			{
				debugPrintLn("recv failed");
			}
		}
		else
		{
		  debugPrintLn("No reply, is server alive?");
		}

		debugSerialFlush();
	}
	exit(0);
}

static void smartdelay(unsigned long ms)
{
  unsigned long start = millis();
  do
  {
    while (ss.available())
      gps.encode(ss.read());
  } while (millis() - start < ms);
}

