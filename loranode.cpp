#include "loranode.h"

struct loranode::band loranode::my_band[NUM_BANDS];

// Singleton instance of the radio driver and LED pin
#ifdef ESP8266
RH_RF95 rf95(D8,D1);
#define LED BUILTIN_LED
#else if ATM328
RH_RF95 rf95(10, 2);
#define LED 13
#endif

loranode::loranode(){

	// initialise default values
	band_id = g;
	channel = 0;
	bw_ih_conf = RH_RF95::Bw125_Header;
	sf_conf = RH_RF95::Sf12_CRC;
	ldro_agc_conf = RH_RF95::LDROoff_AGCauto;
	power = 5;
}

loranode::loranode(uint8_t bid, uint8_t chan, RH_RF95::BW_IH_t bw_ih_c, RH_RF95::SF_t sf_c, RH_RF95::LDRO_AGC_t ldro_agc_c, uint8_t pow){

	// initialize specific values
	band_id = bid;
	channel = chan;
	bw_ih_conf = bw_ih_c;
	sf_conf = sf_c;
	ldro_agc_conf = ldro_agc_c;
	power = pow;
}

void loranode::init(){

	// setup bands
	for (uint8_t i=0;i<NUM_BANDS;i++){

		loranode::setup_band(i, band_start_freq[i], band_num_channels[i], 0.2);
	}

	if (!rf95.init()){
		debugPrintLn("init failed");
	}

	debugPrint("Initial bw_ih_conf = ");
	debugPrintLn(bw_ih_conf);
	setBwIhConf(bw_ih_conf);

	debugPrint("Initial sf_conf = ");
	debugPrintLn(sf_conf);
	setSfConf(sf_conf);

	debugPrint("Initial ldro_agc_conf = ");
	debugPrintLn(ldro_agc_conf);
	setLdroAgcConf(ldro_agc_conf);

	debugPrint("Initial power = ");
	debugPrintLn(power);
	setPower(power);

	debugPrint("Initial freq = ");
	debugPrintLn(my_band[band_id].channel_freq[channel]);
	setFreq(band_id, channel);
}

void loranode::setup_band(uint8_t band_id, float start_freq, uint8_t num_channels, float channel_spacing_mhz){

	loranode::my_band[band_id].band_id = band_id;
	loranode::my_band[band_id].num_channels = num_channels;

	loranode::my_band[band_id].channel_freq = (float*)malloc(num_channels*sizeof(float));

	for (int i=0; i<num_channels; i++){
		loranode::my_band[band_id].channel_freq[i] = start_freq + i*channel_spacing_mhz;
	}
}

uint8_t loranode::getBandId() const {
	return band_id;
}

void loranode::setFreq(uint8_t bandId, uint8_t chan) {
	band_id = bandId;
	channel = chan;

	// Set freq
	rf95.setFrequency( my_band[band_id].channel_freq[channel] );
}

uint8_t loranode::getChannel() const {
	return channel;
}

uint8_t loranode::getPower() const {
	return power;
}

void loranode::setPower(uint8_t pow) {
	power = pow;

	// Set Tx Power,dBm
	 // from 5 to 23 dBm using PA_BOOST for RFM95/96/97/98 modules
	rf95.setTxPower( power, false );
}

void loranode::setNextSf(){

	int temp_sf_conf = sf_conf;

    if (--temp_sf_conf >= 1){
    	setSfConf((RH_RF95::SF_t)temp_sf_conf);
    }
    else{
    	setSfConf(RH_RF95::Sf12_CRC);
    }
}

RH_RF95::BW_IH_t loranode::getBwIhConf() const {
	return bw_ih_conf;
}

void loranode::setBwIhConf(RH_RF95::BW_IH_t bwIhConf) {
	bw_ih_conf = bwIhConf;
	rf95.setBW_IH(bw_ih_conf);
}

RH_RF95::LDRO_AGC_t loranode::getLdroAgcConf() const {
	return ldro_agc_conf;
}

void loranode::setLdroAgcConf(RH_RF95::LDRO_AGC_t ldroAgcConf) {
	ldro_agc_conf = ldroAgcConf;
	rf95.setLDRO_AGC(ldro_agc_conf);
}

RH_RF95::SF_t loranode::getSfConf() const {
	return sf_conf;
}

void loranode::setSfConf(RH_RF95::SF_t sfConf) {
	sf_conf = sfConf;

	//rf95.setSF6Ops( (sf_conf == RH_RF95::Sf6_CRC) );
	rf95.setSf(sf_conf);
}
